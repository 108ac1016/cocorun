﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleAnim : MonoBehaviour
{
    private Animation anim;
    string AnimName;

    private void OnEnable()
    {
        anim = this.gameObject.GetComponent<Animation>();
        anim.Play("title");
        
    }
    // Start is called before the first frame update
    void Start()
    {
        anim.CrossFadeQueued("doudou");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

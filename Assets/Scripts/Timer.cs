﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Timers;

public class Timer : MonoBehaviour
{
    int CountDown = 2;
    public Text clock;
    public Text countdown;

    void Start()
    {
        StartCoroutine(Counts());
        General_Item.time = 0;
    }

    private void FixedUpdate()
    {
        ClockTick();
    }

    void ClockTick()
    {
        if(General_Item.run)
        {
            General_Item.time += Time.deltaTime;
            clock.text = Mathf.Round(General_Item.time).ToString();
        }
    }

    public static void ClockStop()
    {
        General_Item.run = false;
    }
    public static  void ClockStart()
    {
        General_Item.run = true;
    }
    IEnumerator Counts()
    {
        countdown.text = CountDown.ToString();
        float Counting = 0;
        while(Counting < CountDown)
        {
            Counting += Time.deltaTime;
            yield return null;
        }
        ClockStart();
    }
}

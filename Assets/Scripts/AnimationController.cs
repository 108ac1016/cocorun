﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Animation anim;
    private string CurrentAnimation_Name;

    void OnEnable()
    {
        if (anim == null)
        {
            anim = this.GetComponent<Animation>();
            anim.Play("coco_idle");
            CurrentAnimation_Name = "idle";
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            if(CurrentAnimation_Name != "run")
            {
                CurrentAnimation_Name = "walk";
                anim.CrossFade("coco_walk");
            }  
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CurrentAnimation_Name = "jump";
            anim.CrossFade("coco_jump");
        }

        if (!(Input.anyKey))
        {
            if(CurrentAnimation_Name == "walk")
            {
                anim.CrossFade("coco_idle", 0.2f);
            }
            else
            {
                anim.CrossFadeQueued("coco_idle", 0.1f);
            }
            CurrentAnimation_Name = "idle";
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randommud : MonoBehaviour
{
    static public bool MudBorn;

    public GameObject mudskipper1,mudskipper2;
    public Transform pos1,pos2,pos3,pos4,pos5,pos6;
    public AudioSource au;
    public AudioClip tantuuu;
    public float t = 0f;
    public float speed = 100f;
    public int next = 1;
    float posplus;
    float randomx,randomy;
    // Start is called before the first frame update
    void Start()
    {
        MudBorn = false;
    }

    // Update is called once per frame
    void Update()
    {
        t = Time.time;
        randomx = Random.Range(0f, 3f);
        randomy = Random.Range(0f, 1f);

        if (MudBorn)
        {
            if (t > next)
            {  
                if (next % 6 == 0)
                {
                    GameObject bulletspawn = Instantiate(mudskipper1, pos1.position + new Vector3(posplus, randomy, 0), mudskipper1.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, -speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else if (next % 6 == 2)
                {
                    GameObject bulletspawn = Instantiate(mudskipper1, pos2.position + new Vector3(posplus, randomy, 0), mudskipper2.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, -speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else if (next % 6 == 4)
                {
                    GameObject bulletspawn = Instantiate(mudskipper1, pos3.position + new Vector3(posplus, randomy, 0), mudskipper2.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, -speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else if (next % 6 == 1)
                {
                    GameObject bulletspawn = Instantiate(mudskipper2, pos4.position + new Vector3(posplus, randomy, 0), mudskipper2.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else if (next % 6 == 3)
                {
                    GameObject bulletspawn = Instantiate(mudskipper2, pos5.position + new Vector3(posplus, randomy, 0), mudskipper2.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else if (next % 6 == 5)
                {
                    GameObject bulletspawn = Instantiate(mudskipper2, pos6.position + new Vector3(posplus, randomy, 0), mudskipper2.transform.rotation);
                    bulletspawn.GetComponent<Rigidbody>().velocity = new Vector3(posplus - randomx, randomy, speed);
                    next++;
                    au.clip = tantuuu;
                    au.Play();
                }
                else
                {
                    next++;
                }
                Debug.Log("randomx:" + randomx);
                Debug.Log("randomy:" + randomy);
            }
        }
        else if (!MudBorn)
        {
            next = (int)t + 1;
        }

    }
}

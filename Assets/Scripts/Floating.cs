﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    [SerializeField]
    float UpDown;
    [SerializeField]
    float Speed;
    [SerializeField]
    float Second;

    float Different;
    int PlusMinus;  // 0 = positive 1 = negative
    Vector3 FloatRange;
    Vector3 Original;
    bool Moving;

    // Start is called before the first frame update
    void Start()
    {
        Original = this.transform.position;
    }

    private void FixedUpdate()
    {
        if (this.transform.position.y == Original.y)
        {
            RandomFloat();
            Moving = true;
        }

        if (Moving)
        {
            if (FloatRange.y > this.transform.position.y)
            {
                this.transform.position += ((FloatRange - this.transform.position) * Speed) * Time.deltaTime;

                if ((FloatRange.y - this.transform.position.y) < (Different / Second))
                {
                    Moving = false;
                }
            }
            else if (FloatRange.y < this.transform.position.y)
            {
                this.transform.position -= ((this.transform.position - FloatRange) * Speed) * Time.deltaTime;
                if ((this.transform.position.y - FloatRange.y) < (Different / Second))
                {
                    Moving = false;
                }
            }
        }
        else if(!Moving)
        {
            if (this.transform.position.y > Original.y)
            {
                this.transform.position -= ((this.transform.position - Original) * Speed) * Time.deltaTime;
                if ((this.transform.position.y - Original.y) < (Different / Second))
                {
                    RandomFloat();
                    Moving = true;
                }
            }
            else if (this.transform.position.y < Original.y)
            {
                this.transform.position += ((Original - this.transform.position) * Speed) * Time.deltaTime;
                if ((Original.y - this.transform.position.y) < (Different / Second))
                {
                    RandomFloat();
                    Moving = true;
                }
            }
        }
        

        
    }
        

    void RandomFloat()
    {
        PlusMinus = Random.Range(0, 1);

        if(PlusMinus == 0)
        {
            FloatRange = Original + new Vector3(0, Random.Range(0, UpDown), 0);
        }
        else if(PlusMinus == 1)
        {
            FloatRange = Original - new Vector3(0, Random.Range(0, UpDown), 0);
        }
        Different = Mathf.Abs(FloatRange.y - this.transform.position.y);
    }
}

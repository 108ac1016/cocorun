﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject Player;
    public float Mouse_Sensitive = 100f;
    public float Distance = 4.5f;
    public float Coco_Height = 0.6f;
    public float OrbitDampling = 10f;
    float Distance_Temp = 0f;

    Transform Player_Transform;
    Transform Camera_Transform;
    protected Vector3 offset;
    Vector3 Distance_Change;
    

    protected float Mouse_X;
    protected float Mouse_Y;

    // Start is called before the first frame update
    void Start()
    {
        Player_Transform = Player.GetComponent<Transform>();
        Camera_Transform = this.GetComponent<Transform>();
        offset = Camera_Transform.position - Player_Transform.position;
    }

    private void LateUpdate()
    {
        Coco_Height = Mathf.Clamp(Coco_Height, 0.1f, 1.0f);
        Mouse_Sensitive = Mathf.Clamp(Mouse_Sensitive, 80f, 120f);
        IWantYou();

        if (Player != null)
        {
            if (Distance_Temp != Distance)
            {
                Distance = Mathf.Clamp(Distance, 4f, 5f);
                Distance_Change = new Vector3(0, 0, -Distance);
                Distance_Temp = Distance;
            }

            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                Mouse_X += Input.GetAxis("Mouse X") * Mouse_Sensitive * Time.deltaTime;
                Mouse_Y -= Input.GetAxis("Mouse Y") * Mouse_Sensitive * Time.deltaTime;
            }
            Mouse_Y = Mathf.Clamp(Mouse_Y, 0f, 90f);

            this.transform.rotation = Quaternion.Euler(Mouse_Y, Mouse_X, 0);
            //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(Mouse_Y, Mouse_X, 0), Time.deltaTime * OrbitDampling);
            this.transform.position = Player_Transform.position + new Vector3(0, Coco_Height, 0) + Quaternion.Euler(Mouse_Y, Mouse_X, 0) * Distance_Change;
        }
       
;    }
    // Update is called once per frame
    void Update()
    {

        
    }

    void IWantYou()
    {
        if (Player == null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
            Player_Transform = Player.GetComponent<Transform>();
            Camera_Transform = this.GetComponent<Transform>();
            offset = Camera_Transform.position - Player_Transform.position;
        }
    }
}

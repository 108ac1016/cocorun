﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public GameObject BackgroundMusic;

    private void Awake()
    {
        BackgroundMusic = GameObject.FindGameObjectWithTag("Music");
        if(BackgroundMusic == null)
        {
            BackgroundMusic = GameObject.FindGameObjectWithTag("BackgroundMusic");
            //DontDestroyOnLoad(BackgroundMusic);
        }
        BackgroundMusic.tag = "Music";
    }
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Destroy(GameObject.FindGameObjectWithTag("BackgroundMusic"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

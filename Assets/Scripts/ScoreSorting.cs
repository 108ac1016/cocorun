﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSorting : MonoBehaviour
{
    public Text First, Second, Third;
    // Start is called before the first frame update
    void Start()
    {
        ScoreCompare();
        Rank();
    }

    void ScoreCompare()
    {
        if (General_Item.last_Time < General_Item.third || General_Item.third == 0)
        {
            if (General_Item.last_Time < General_Item.second || General_Item.second == 0)
            {
                if (General_Item.last_Time < General_Item.first || General_Item.first == 0)
                {
                    General_Item.third = General_Item.second;
                    General_Item.second = General_Item.first;
                    General_Item.first = General_Item.last_Time;
                }
                else
                {
                    General_Item.third = General_Item.second;
                    General_Item.second = General_Item.last_Time;
                }
            }
            else
            {
                General_Item.third = General_Item.last_Time;
            }
        }
    }

    void Rank()
    {
        First.text = General_Item.first.ToString();
        Second.text = General_Item.second.ToString();
        Third.text = General_Item.third.ToString();
    }
}

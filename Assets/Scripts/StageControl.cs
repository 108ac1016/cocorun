﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageControl : MonoBehaviour
{
    public Camera Cam;
    public float Times;
    bool UnActive;
    float TimePass;
    public GameObject Nav;

    private void Start()
    {
        int index = SceneManager.GetActiveScene().buildIndex;
        switch (index)
        {
            case 0:
                Cursor.visible = true;
                break;
            case 1:
                Cursor.visible = false;
                break;
            case 2:
                Cursor.visible = false;
                break;
            case 3:
                Cursor.visible = true;
                break;
        }
        UnActive = true;
        TimePass = 0;
        CameraExtension.FadeIn(Cam, Times);
    }
    // Update is called once per frame
    void Update()
    {
        TimePass += Time.deltaTime;
        if (UnActive)
        {
            if (TimePass >= 2)
            {
                Nav.SetActive(true);
                UnActive = false;
            }
        }
        
        Cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    private void OnCollisionEnter(Collision collision)
    {  
        if (collision.gameObject.tag == "Player")
        {
            if (this.tag == "StageTwo")
            {
                LifeControl.Respawn = GameObject.FindGameObjectWithTag("RespawnSecond");
                randommud.MudBorn = true;
            }
            else
            {
                Destroy(Nav);
                General_Item.run = false;
                General_Item.last_Time = General_Item.time;
                CameraExtension.FadeOut(Cam, Times - 0.5f);
                StartCoroutine(Delay(Times));
            }
            
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("cocoworld");
    }

    public void EndGame()
    {
        SceneManager.LoadScene("cocostart");
    }

    IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
        switch (this.tag)
        {
            case "IntoMain":
                SceneManager.LoadScene("incroco");
                break;
            case "IntoEnding":
                SceneManager.LoadScene("cocoend");
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeControl : MonoBehaviour
{
    public GameObject REPlayer;
    static public GameObject Respawn;
    GameObject NowPlaying;


    // Start is called before the first frame update
    void Start()
    {
        NowPlaying = GameObject.FindGameObjectWithTag("Player");
        Respawn = GameObject.FindGameObjectWithTag("RespawnFirst");
    }

    // Update is called once per frame
    void Update()
    {
        if (NowPlaying == null)
        {
            NowPlaying = Instantiate(REPlayer, Respawn.transform.position, Quaternion.identity);
        }
    }
}

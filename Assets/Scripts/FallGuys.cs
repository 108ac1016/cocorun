﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallGuys : MonoBehaviour
{
    public AudioSource au;
    public AudioClip newtrack2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            au.clip = newtrack2;
            au.Play();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            au.clip = newtrack2;
            au.Play();
        }
    }
}

# 大二上程式設計專題


##  專題名稱：COCORUN

#### 組員: 林詩涵、林辰、廖祐凜

### 程式說明:

COCORUN是一款3D平台遊戲，主角是一隻黃色的胖小魚Coco，他追著寶藏來到了一隻張著大嘴的鱷魚前，沒有注意的一不小心進到了鱷魚的大嘴裡，在鱷魚的肚子裡到處都是危險的胃酸，玩家要做的就是在鱷魚體內的組織上小心跳躍，避免掉進牠的胃酸，到達最後的寶藏。

### 技術說明：

* Unity 2019.4.10

### 遊戲畫面 
#### 標題與主視覺
###### (點擊標題開啟影片)

[![](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/01_title.JPG)](https://youtu.be/o3xvFIVIwws)

![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/02_main.JPG)
#### 角色介紹
![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/03_chara_design.JPG)
![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/04_chara_info.JPG)
#### 故事
![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/05_story_1.JPG)
![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/06_story_2.JPG)
![image](https://gitlab.com/108ac1016/cocorun/-/raw/main/Recordings/07_story_3.JPG)
